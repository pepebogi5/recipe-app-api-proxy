# Recipe App API Proxy

NGINX proxy app for our proxy API

## Usage

### Environment variables

 * `LISTEN_PORT` - Port to listen on (default `3000`)
 * `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
 * `APP_PORT` - Port of the app to forward requests to (default: `9000`)
